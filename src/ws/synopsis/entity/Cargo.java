package ws.synopsis.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name="Cargo.findAll",
                query="SELECT t FROM Cargo t"),
    
    @NamedQuery (name="Cargo.getById",
    query = "SELECT t FROM Cargo t WHERE t.id = :t_codigo"),
    
    //@NamedQuery(name="Cargo.getByNombre",
	//query="SELECT t FROM Cargo t WHERE t.nombre = :t_nombre"),
}) 
@Table(name="tb_cargo")
public class Cargo implements Serializable{
	

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="nombre_cargo", nullable=false, length=500)
	private String nombreCargo;

	
	//Constructor
	//=================================
	
	public Cargo(){}
	
	public Cargo (int id, String nombre){
		super();
		this.id= id;
		this.nombreCargo = nombre;
		
		
	}
	
	
	
	// Metodos get and set
	//==================================
				
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombreCargo() {
		return nombreCargo;
	}

	public void setNombreCargo(String nombreCargo) {
		this.nombreCargo = nombreCargo;
	}
	
	
	

				
}
