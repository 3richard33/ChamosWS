package ws.synopsis.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name="Proyectos.findAll",
                query="SELECT o FROM Proyectos o"),
    
    @NamedQuery(name="Proyectos.getByNombreProyecto",
    			query= "SELECT h FROM Proyectos h WHERE h.nombre_proyecto = :p_nombre"),
}) 
@Table(name="tb_proyectos")
public class Proyectos implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; //autogenerado
	
	@Column(name="nombre_proyecto", nullable=false, length=500)
	private String nombre_proyecto;
	
	
	@ManyToOne
	@JoinColumn(name="personal_id")
	private Personal id_personal;

	
	//constructor
	//===========================
	
	public Proyectos(){}
	
	public Proyectos(int id, String nombreproyecto, Personal idpersonal){
		super();
		
		this.id= id;
		this.nombre_proyecto= nombreproyecto;
		this.id_personal= idpersonal;
	}
	
	

	// Metodos get and set
		//==================================
		
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre_proyecto() {
		return nombre_proyecto;
	}


	public void setNombre_proyecto(String nombre_proyecto) {
		this.nombre_proyecto = nombre_proyecto;
	}


	public Personal getId_personal() {
		return id_personal;
	}


	public void setId_personal(Personal id_personal) {
		this.id_personal = id_personal;
	}
	
	
	
	
}
