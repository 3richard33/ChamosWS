package ws.synopsis.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name="Personal.findAll",
                query="SELECT p FROM Personal p"),
   
    @NamedQuery(name="Personal.getByDni",
    			query="SELECT p FROM Personal p WHERE p.dni= :p_dni"),
    
}) 
@Table(name="tb_personal")
public class Personal implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="dni", nullable=false, length=20)
	private String dni;
	
	@Column(name="nombre", nullable=false, length=500)
	private String nombre;
	
	@Column(name="apellido", nullable=false, length=500)
	private String apellido;
	
	@Column(name="direccion", nullable=false, length=500)
	private String direccion;
			
	@ManyToOne
	@JoinColumn(name="id_cargo")
	private Cargo id_cargo;
	
	
	//constructor 
	//================
	
	public Personal(){}
	
	public Personal(int id, String dni, String nombre, String apellido, String direccion,Cargo idcargo){
		
		super();
		this.id= id;
		this.dni=dni;
		this.nombre=nombre;
		this.apellido=apellido;
		this.direccion=direccion;
		this.id_cargo= idcargo;
		
	}
	
	

	// Metodos get and set
	//==================================
		


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Cargo getId_cargo() {
		return id_cargo;
	}

	public void setId_cargo(Cargo id_cargo) {
		this.id_cargo = id_cargo;
	}

	

	
}