package ws.synopsis.managedBean;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import ws.synopsis.cnx.JPAUtil;
import ws.synopsis.entity.Cargo;


public class CargoMB {
	
	public	CargoMB(){
		
		System.out.println("llego a CargoMB");
	}
	
	//========Metodo de Consulta=============
	
	public List<Cargo> listaCargo(){
		EntityManager entity = JPAUtil.getEntityManager();
		TypedQuery<Cargo> consulta=entity.createNamedQuery("Cargo.findAll", Cargo.class);
		return consulta.getResultList();
	}
	
	public Cargo getCargoByNombre(String nombre ){
		EntityManager em=JPAUtil.getEntityManager();
		TypedQuery<Cargo> consulta=em.createNamedQuery("Cargo.getByNombre", Cargo.class);
		consulta.setParameter("t_nombre", nombre);
		return consulta.getSingleResult();		
	}
	
	
	// ========= METODOS DE CRUD ============
	
	
	public boolean insertar(Cargo cargo) {
		EntityManager manager = null;
		try {
			manager = JPAUtil.getEntityManager();
			manager.getTransaction().begin();
			manager.persist(cargo);// Genera el JPQL internamente
			manager.flush();// Enviar en cola(pueden haber varios SQL)
			manager.getTransaction().commit(); // Envia a la BD
			System.out.println("Se Registro un nuevo Cargo");
			return true;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			return false; 
		} finally {
			manager.close();
		}
	}
	
	public boolean actualizar(Cargo cargo, int id) {
		cargo.toString();
		EntityManager manager = null;
		try {
			if (cargo!= null) {
				cargo.setId(id); 
				cargo.toString();
			}
			System.out.println("\n Cargo a actualizar, codigo: " + cargo.getId());
			manager = JPAUtil.getEntityManager();
			manager.getTransaction().begin();
			manager.merge(cargo);
			manager.flush();
			manager.getTransaction().commit();
			System.out.println("Se Actualizo un Cargo, con el codigo: " + cargo.getId());
			return true;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			manager.close();
		}		
	}
	
	

	public void eliminar(int id) {
		EntityManager manager = null;
		System.out.println("llego el metodo eliminar");
		try {
			manager = JPAUtil.getEntityManager();
			Cargo c = manager.find(Cargo.class, id);
			manager.getTransaction().begin();
			manager.remove(c);
			manager.flush();
			manager.getTransaction().commit();
			System.out.println("Se Elimino un cargo, con el codigo: " + id );			
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
}
