package ws.synopsis.managedBean;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import ws.synopsis.cnx.JPAUtil;

import ws.synopsis.entity.Proyectos;

public class ProyectosMB  {
	
	public ProyectosMB(){
		System.out.println("llego a ProyectosMB");
		
	}

	//========Metodo de Consulta=============

	public List<Proyectos> listaProyectos(){
		EntityManager entity = JPAUtil.getEntityManager();
		TypedQuery<Proyectos> consulta=entity.createNamedQuery("Proyectos.findAll", Proyectos.class);
		return consulta.getResultList();
}
	public Proyectos getByNombreProyecto(String nombre_proyectos){
		EntityManager em = JPAUtil.getEntityManager();
		TypedQuery<Proyectos> consulta= em.createNamedQuery("Proyectos.getByNombreProyecto", Proyectos.class);
		consulta.setParameter("p_nombre", nombre_proyectos);
		
		return consulta.getSingleResult();	
	}
	
	
	// ========= METODOS DE CRUD ============
	
	
		public boolean insertar(Proyectos proyecto) {
			EntityManager manager = null;
			try {
				manager = JPAUtil.getEntityManager();
				manager.getTransaction().begin();
				manager.persist(proyecto);// Genera el JPQL internamente
				manager.flush();// Enviar en cola(pueden haber varios SQL)
				manager.getTransaction().commit(); // Envia a la BD
				System.out.println("Se Registro un nuevo Proyecto");
				return true;
			} catch (Exception e) {
				manager.getTransaction().rollback();
				e.printStackTrace();
				return false; 
			} finally {
				manager.close();
			}
		}
		
		public boolean actualizar(Proyectos proyecto) {
			proyecto.toString();
			EntityManager manager = null;
			try {
				/*if (proyecto!= null) {
					proyecto.setId(id); 
					proyecto.toString();
				}*/
				
				//manager.find(Proyectos.class,proyecto);
				//Proyectos proyectoauxi = manager.find(Proyectos.class,proyecto); 
				//if (proyectoauxi == null){
					
					// hay una clase de evalauciones 
				//}
				
				System.out.println("\n proyecto a actualizar, codigo: " + proyecto.getId());
				manager = JPAUtil.getEntityManager();
				manager.getTransaction().begin();
				manager.merge(proyecto);
				manager.flush();
				manager.getTransaction().commit();
				System.out.println("Se Actualizo un proyecto, con el codigo: " + proyecto.getId());
				return true;
			} catch (Exception e) {
				manager.getTransaction().rollback();
				e.printStackTrace();
				return false;
			} finally {
				manager.close();
			}		
		}
		
		

		public void eliminar(int id) {
			EntityManager manager = null;
			try {
				manager = JPAUtil.getEntityManager();
				Proyectos p = manager.find(Proyectos.class, id);
				manager.getTransaction().begin();
				manager.remove(p);
				manager.flush();
				manager.getTransaction().commit();
				System.out.println("Se Elimino un proyectos, con el codigo: " + id );			
			} catch (Exception e) {
				manager.getTransaction().rollback();
				e.printStackTrace();
			} finally {
				manager.close();
			}
		}
	
	
}