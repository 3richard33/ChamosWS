package ws.synopsis.managedBean;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import ws.synopsis.cnx.JPAUtil;

import ws.synopsis.entity.Personal;


public class PersonalMB {


		public	PersonalMB(){
		
		System.out.println("llego a PersonalMB");
	}
	
		//========Metodo de Consulta=============
	
	public List<Personal> listaPersonal(){
		EntityManager entity = JPAUtil.getEntityManager();
		TypedQuery<Personal> consulta=entity.createNamedQuery("Personal.findAll", Personal.class);
		return consulta.getResultList();
	}
	
	public Personal getPersonalByDNI(String dni){
		EntityManager em = JPAUtil.getEntityManager();
		TypedQuery<Personal> consulta= em.createNamedQuery("Personal.getByDni", Personal.class);
		consulta.setParameter("p_dni", dni);
		
		return consulta.getSingleResult();	
	}
	
	
	// ========= METODOS DE CRUD ============
	
	
	public boolean insertar(Personal persona) {
		EntityManager manager = null;
		try {
			manager = JPAUtil.getEntityManager();
			manager.getTransaction().begin();
			manager.persist(persona);// Genera el JPQL internamente
			manager.flush();// Enviar en cola(pueden haber varios SQL)
			manager.getTransaction().commit(); // Envia a la BD
			System.out.println("Se Registro un nuevo Personal");
			return true;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			return false; 
		} finally {
			manager.close();
		}
	}
	
	public boolean actualizar(Personal persona, int id) {
		persona.toString();
		EntityManager manager = null;
		try {
			if (persona!= null) {
				persona.setId(id); 
				persona.toString();
			}
			System.out.println("\n Personal a actualizar, codigo: " + persona.getId());
			manager = JPAUtil.getEntityManager();
			manager.getTransaction().begin();
			manager.merge(persona);
			manager.flush();
			manager.getTransaction().commit();
			System.out.println("Se Actualizo un Personal, con el codigo: " + persona.getId());
			return true;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			manager.close();
		}		
	}
	
	

	public void eliminar(int id) {
		EntityManager manager = null;
		try {
			manager = JPAUtil.getEntityManager();
			Personal p = manager.find(Personal.class, id);
			manager.getTransaction().begin();
			manager.remove(p);
			manager.flush();
			manager.getTransaction().commit();
			System.out.println("Se Elimino un personal, con el codigo: " + id );			
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			manager.close();
		}
	}

}
