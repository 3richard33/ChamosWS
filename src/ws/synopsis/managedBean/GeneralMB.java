package ws.synopsis.managedBean;

import javax.persistence.EntityManager;

import ws.synopsis.cnx.JPAUtil;


public class GeneralMB {
	
	public <T> Object buscar(Long id,Class<T> clase) {
		EntityManager manager = null;
		try {
			manager = JPAUtil.getEntityManager();
			return manager.find(clase,id);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			manager.close();
		}
		return null;
	}
	
	
	public void insertar(Object obj){
		
		EntityManager manager = null;

		try {
			manager = JPAUtil.getEntityManager();
			manager.getTransaction().begin();
			manager.persist(obj);//Genera el JPQL internamente 
			manager.flush();// Enviar en cola(pueden haber varios SQL)
			manager.getTransaction().commit(); //Envia a la BD
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			manager.close();
		}
	}
	
	
	
	public <T> void eliminar(Long id,Class<T> clase) {
		EntityManager manager = null;

		try {
			manager = JPAUtil.getEntityManager();
			Object c = manager.find(clase,id);
			
			manager.getTransaction().begin();
			manager.remove(c);
			manager.flush();
			manager.getTransaction().commit(); 
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			manager.close();
		}
	}
	
	
	
	public void actualizar(Object obj) {
		EntityManager manager = null;

		try {
			manager = JPAUtil.getEntityManager();
			manager.getTransaction().begin();
			manager.merge(obj);
			manager.flush();
			manager.getTransaction().commit(); 
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			manager.close();
		}
	}
	

}
