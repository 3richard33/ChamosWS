package ws.synopsis.servicios.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ws.synopsis.entity.Cargo;
import ws.synopsis.entity.Personal;
import ws.synopsis.entity.Proyectos;
import ws.synopsis.managedBean.CargoMB;
import ws.synopsis.managedBean.PersonalMB;
import ws.synopsis.managedBean.ProyectosMB;

@Path("/API-POST")
public class ServiciosPOST {
	
	@POST
	@Path("/Cargo/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean registroCargo(Cargo carg) throws Exception{
		
		CargoMB cargoMB = new CargoMB();
		
		return cargoMB.insertar(carg);		
	}
	
	
	@POST
	@Path("/Personal/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean registrarPersonal(Personal person) throws Exception{
		
		PersonalMB personalMB = new PersonalMB();
		
		return personalMB.insertar(person);
		
		
	}
	
	@POST
	@Path("/Proyectos/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean registrarProyectos(Proyectos proyect){
		
		ProyectosMB proyectosMB = new ProyectosMB(); 
		
		return proyectosMB.insertar(proyect);
		
		
	}
	

}
