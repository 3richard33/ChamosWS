package ws.synopsis.servicios.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ws.synopsis.entity.Cargo;
import ws.synopsis.managedBean.CargoMB;
import ws.synopsis.entity.Personal;
import ws.synopsis.entity.Proyectos;
import ws.synopsis.managedBean.PersonalMB;
import ws.synopsis.managedBean.ProyectosMB;



@Path("/API_PUT")
public class ServicioPUT {
	
	//**************************SERVICIOS************************
	//localhost:8080/
	
	@PUT
	@Path("/Cargo/{id}/{nombre}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean actualizarCargo(
			@PathParam("id") String id,
			@PathParam("nombre") String nombre)throws Exception {
		
		CargoMB cargoMB = new CargoMB();
		Cargo carg = new Cargo();
		carg.setId(Integer.parseInt(id));
		carg.setNombreCargo(nombre);
		
		return cargoMB.actualizar(carg, Integer.parseInt(id));
		
		
	}
	
	
	@PUT
	@Path("/Personal/{id}/{dni}/{nombre}/{apellido}/{direccion}/{id_cargo}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean actualizarPersonal(
			@PathParam("id")String id,
			@PathParam("dni") String dni,
			@PathParam("nombre") String nombre,
			@PathParam("apellido") String apellido,
			@PathParam("direccion") String direccion,
			@PathParam("id_cargo") Cargo id_cargo) throws Exception{
		
		PersonalMB personalMB = new PersonalMB();
		Personal pers= new Personal();
		pers.setId(Integer.parseInt(id));
		pers.setDni(dni);
		pers.setNombre(nombre);
		pers.setApellido(apellido);
		pers.setDireccion(direccion);
		pers.setId_cargo( id_cargo);
		
		return personalMB.actualizar(pers,Integer.parseInt(id));
		
		
	}
		

	@PUT
	@Path("/Proyectos/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean actualizarProyectos(Proyectos proyect) throws Exception{
		
		ProyectosMB proyectosMB = new ProyectosMB();
		//Proyectos proyect = new Proyectos();
		
		
		return proyectosMB.actualizar(proyect);
	}
	
}
	
	

