package ws.synopsis.servicios.rest;



import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ws.synopsis.managedBean.CargoMB;
import ws.synopsis.managedBean.PersonalMB;
import ws.synopsis.managedBean.ProyectosMB;

@Path("/API-DELETE")
public class ServicioDelete {
	//***************************Servicio*********************
	//localhost:8080/
	
	@DELETE
	@Path("/Cargo/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void eliminarCargo(@PathParam("id") int id){
		
		CargoMB cargoMB = new CargoMB();
		cargoMB.eliminar(id);		
	}
	
	//***********************************servicio Personal************
	@DELETE 
	@Path("/Personal/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void eliminarPersonal(@PathParam("id") int id){
		
		PersonalMB personalMB = new PersonalMB();
		personalMB.eliminar(id);	
	}
	
	//******************servicio proyectos***************************
	
	@DELETE
	@Path("/Proyectos/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void eliminarProyectos(@PathParam("id") int id ){
		
		ProyectosMB proyectosMB = new ProyectosMB();
		proyectosMB.eliminar(id);
		
	}

}
