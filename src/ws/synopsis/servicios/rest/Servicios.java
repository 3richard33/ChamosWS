package ws.synopsis.servicios.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ws.synopsis.entity.Cargo;
import ws.synopsis.entity.Personal;
import ws.synopsis.entity.Proyectos;
import ws.synopsis.managedBean.CargoMB;
import ws.synopsis.managedBean.PersonalMB;
import ws.synopsis.managedBean.ProyectosMB;




@Path("/API")
public class Servicios {
	
	
	
	//*********************************HEROES*****************************
	//localhost:8080/SynopsisWS/API/listaPersonal
		@GET
		@Path("/listaPersonal")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Personal>listaPersonalJson(){
			PersonalMB personalMB = new PersonalMB();
			return personalMB.listaPersonal();
		}
		
	//*********************************JUGADORES*****************************
	//localhost:8080/SynopsisWS/API/listaCargo
		@GET
		@Path("/listaCargo")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Cargo>listaCargoJson(){
			CargoMB cargoMB = new CargoMB();
			return cargoMB.listaCargo();
		}
					
		
	
	//*********************************OBJETOS*****************************
	//localhost:8080/SynopsisWS/API/listaProyectos
		@GET
		@Path("/listaProyectos")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Proyectos>listaProyectosJson(){
			ProyectosMB proyectosMB = new ProyectosMB();
			return proyectosMB.listaProyectos();
		}
		
		//*********************************SETS*****************************
	
}
